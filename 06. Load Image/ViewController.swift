//
//  ViewController.swift
//  06. Load Image
//
//  Created by x0000ff on 17/07/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//#######################################################################
import UIKit

//#######################################################################
class ViewController: UIViewController {

  //#######################################################################
  @IBOutlet weak var owlImageView: UIImageView!

  //#######################################################################
  @IBAction func loadImageTapped(sender: AnyObject) {
    
    let image = UIImage(named: "owl")
    
    // Вуаля!
    owlImageView.image = image
    
  }
  
  //#######################################################################
}

